package inscricao.cdi.beans;

import inscricao.entity.Candidato;
import inscricao.entity.Idioma;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import utfpr.faces.support.PageBean;

/**
 *
 * @author Wilson
 */
@Named
@RequestScoped
public class InscricaoBean extends PageBean {
    
    @Inject
    private IdiomasListBean idiomasBean;
    
    private Candidato candidato = new Candidato(new Idioma());

    public InscricaoBean() {
    }

    public Candidato getCandidato() {
        return candidato;
    }

    public void setCandidato(Candidato candidato) {
        this.candidato = candidato;
    }

    public String confirmaAction() {
        candidato.setDataHora(new Date());
        candidato.setIdioma(idiomasBean.findByCodigo(candidato.getIdioma().getCodigo()));
        return "confirma";
    }
}
